##1.数据库各表间关联关系

以作者（Author）与文章（Article）的关联关系为例：一个作者有多篇文章，任何一篇文章只有唯一一个作者。
在JPA中，一对多的双向关系由多端(Article)来维护。就是说多端(Article)为关系维护端，负责关系的增删改查。
一端(Author)则为关系被维护端，不能维护关系。

-----------> OneToMany
brand（品牌）- product（商品）
   ManyToOne <-------------


-----------> OneToMany
type（类别）- product（商品）
   ManyToOne <-------------


-----------> OneToMany
product（商品） - price（价格）
   ManyToOne <-------------


维护端-----------> OneToOne
price（价格） - supplier（供应商）
    OneToOne <----------被维护端


-----------> OneToMany
department（部门） - employee（员工）
            ManyToOne <-------------


维护端-----------> OneToOne
employee（员工） - users（用户）
   OneToOne <-----------被维护端
当employees表中的数据被删除时，users表中的关联数据也会被删除。

ManyToMany
users(用户) - role(角色) --> rel_user_role

ManyToMany
role(角色) - menu(菜单)  --> rel_role_menu

OneToMany
in_order(入库主表) - in_detail

OneToMany
out_order(出库主表) - out_detail

ManyToMany
product(商品) - warehouse(仓库) --> rel_product_warehouse

维护端-----------------------------------> OneToOne
rel_product_warehouse(商品<->仓库关联) - stock(库存)


##2.timestamp/datetime日期类型兼容问题
1)使用MariaDB数据库时，需要表中除insert_time以外的timestamp或datetime日期默认值修改为 default '0000-00-00 00:00:00'，
2)当使用MySQL(5.7+)时，timestamp/datetime日期的默认值不受日期型字段的数量影响，
所有日期型字段的默认值均可以设置为 default current_timestamp comment


##3.API文档地址：
http://localhost/gpss/swagger-ui.html

##4.需要解决的问题
1.登录成功后在sys_user表的login_time字段写入时间，登出完成以后在sys_user表的logout_time字段写入时间。
实现思路：----------->完成
继承SavedRequestAwareAuthenticationSuccessHandler类，重写onAuthenticationSuccess方法实现登录成功后的操作
实现LogoutSuccessHandler接口，重写onLogoutSuccess方法完成登出后的操作
2.批量新增、删除、更新功能---------->完成

3.复杂查询（动态参数、分页）-------->完成

4.controller返回统一格式的json数据 -------->完成

5.全局统一异常处理 -------->完成

6.具体业务功能 -------->完成

7.ajax跨域请求 -------->完成

8.Controller层注解实现自定义返回json字段 -------->未完成

9.出入库业务库存增加扣减逻辑 -------->未完成
