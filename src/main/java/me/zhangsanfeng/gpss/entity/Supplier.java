package me.zhangsanfeng.gpss.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "base_supplier")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Supplier extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(unique = true,columnDefinition = "int not null comment '编号'")
    private Integer code;

    @Column(columnDefinition = "varchar(100) not null comment '名称'")
    private String name;

    @Column(columnDefinition = "varchar(20) comment '电话'")
    private String telephone;

    @Column(columnDefinition = "varchar(20) comment '联系人'")
    private String contacts;

    @Column(columnDefinition = "varchar(100) comment '地址'")
    private String address;

    /*法人*/
    @Column(name = "legal_person",columnDefinition = "varchar(40) comment '法人代表'")
    private String legalPerson;

    /*税号*/
    @Column(name = "tax_number",columnDefinition = "varchar(20) comment '纳税识别号'")
    private String taxNumber;

    /*开户行*/
    @Column(columnDefinition = "varchar(100) comment '对公账户开户行'")
    private String bank;

    /*对公账户*/
    @Column(columnDefinition = "varchar(20) comment '对公账户账号'")
    private String account;

    /*可根据supplier级联查询price*/
    @OneToOne(mappedBy = "supplier",cascade = {CascadeType.MERGE,CascadeType.REFRESH},optional = false)
    private Price price;
}
