package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * 角色信息
 */
@Entity
@Data
@Table(name = "sys_role")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Role extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(unique = true,columnDefinition = "int not null comment '角色编号'")
    private Integer code;

    @Column(columnDefinition = "varchar(20) not null comment '角色名称'")
    private String name;

    /*用户-角色关联关系：关系被维护端*/
    @ManyToMany(mappedBy = "roleList")
    private List<Users> users;

    /*角色-菜单关联关系：关系维护端，删除role中的记录，将级联删除menu中的关联数据*/
    @ManyToMany
    @JoinTable(name = "rel_role_menu",
            joinColumns = @JoinColumn(name = "role_id",
                    columnDefinition = "varchar(32) not null comment '角色ID'"),
            inverseJoinColumns = @JoinColumn(name = "menu_id",
                    columnDefinition = "varchar(32) not null comment '菜单ID'"))
    private List<Menu> menuList;
}
