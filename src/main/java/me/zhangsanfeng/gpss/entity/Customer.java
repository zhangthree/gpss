package me.zhangsanfeng.gpss.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "base_customer")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Customer extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(unique = true,columnDefinition = "int not null comment '编号'")
    private Integer code;

    @Column(columnDefinition = "varchar(100) not null comment '名称'")
    private String name;

    @Column(columnDefinition = "int(2) default 1 comment '客户级别：数值越大级别越高'")
    private Integer level;

    @Column(columnDefinition = "varchar(20) comment '行业'")
    private String industry;

    @Column(columnDefinition = "varchar(11) comment '手机'")
    private String phone;

    @Column(columnDefinition = "varchar(20) comment '职务'")
    private String duty;

    @Column(columnDefinition = "varchar(20) comment '公司'")
    private String company;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "last_order_date",columnDefinition = "date comment '上次订货日期'")
    private Date lastOrderDate;

    @Column(columnDefinition = "varchar(20) comment '座机'")
    private String telephone;

    @Column(columnDefinition = "varchar(100) comment '地址'")
    private String address;

    /*法人*/
    @Column(name = "legal_person",columnDefinition = "varchar(40) comment '法人代表'")
    private String legalPerson;

    @Column(name = "tax_name",columnDefinition = "varchar(40) comment '发票抬头名称'")
    private String taxName;

    /*税号*/
    @Column(name = "tax_number",columnDefinition = "varchar(20) comment '纳税识别号'")
    private String taxNumber;

    /*开户行*/
    @Column(columnDefinition = "varchar(100) comment '对公账户开户行'")
    private String bank;

    /*对公账户*/
    @Column(columnDefinition = "varchar(20) comment '对公账户账号'")
    private String account;

    @Column(columnDefinition = "varchar(150) comment '备注'")
    private String remarks;

}
