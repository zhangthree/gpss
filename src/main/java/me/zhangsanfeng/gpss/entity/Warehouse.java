package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "base_warehouse")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Warehouse extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(columnDefinition = "int(3) not null comment '编号'")
    private Integer code;

    @Column(columnDefinition = "varchar(100) not null comment '名称'")
    private String name;

    @Column(columnDefinition = "varchar(11) comment '负责人'")
    private String manager;

    @Column(columnDefinition = "varchar(11) comment '联系电话'")
    private String phone;

    @Column(columnDefinition = "varchar(100) comment '仓库地址'")
    private String address;

    /*商品-仓库关联关系：关系被维护端*/
    @ManyToMany(mappedBy = "warehouseList")
    private List<Product> productList;
}
