package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "sys_user")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Users extends BaseEntity implements UserDetails {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(unique = true,columnDefinition = "varchar(11) not null comment '用户名/编码'")
    private String username;

    @Column(columnDefinition = "varchar(100) comment '密码'")
    private String password;

    @Column(name = "login_time",columnDefinition = "datetime on update current_timestamp comment '登录时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date loginTime;

    @Column(name = "logout_time",columnDefinition = "datetime on update current_timestamp comment '退出时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date logoutTime;

    @Column(name = "online_time",columnDefinition = "bigint comment '在线时长'")
    private Long onlineTime;

    @Column(name = "ip_addr",columnDefinition = "varchar(46) comment '登录ip地址'")
    private String ipAddr;

    //@OneToOne(mappedBy = "user",cascade = {CascadeType.MERGE,CascadeType.REFRESH},optional = false)
    //private Employee employee;

    /*用户-角色关联关系：关系维护端，删除user会级联删除role对应的记录*/
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "rel_user_role",
            joinColumns = @JoinColumn(name = "user_id",
                    columnDefinition = "varchar(32) not null comment '用户ID'"),
            inverseJoinColumns = @JoinColumn(name = "role_id",
                    columnDefinition = "varchar(32) not null comment '角色ID'"))
    private List<Role> roleList;

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (roleList == null){
            return null;
        }
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (Role role : roleList) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }



    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }
}
