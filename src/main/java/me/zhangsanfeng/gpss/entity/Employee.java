package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@DynamicUpdate
@Table(name = "base_employee")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Employee extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    /*
    @Column(length = 11,nullable = false,unique = true,columnDefinition = "COMMENT '用户编号：手机号码'")
    private String code;
    */

    @Column(columnDefinition = "varchar(20) not null comment '姓名'")
    private String name;

    @Column(name = "id_card",unique = true,columnDefinition = "varchar(18) not null comment '身份证号'")
    private String idCard;

    @Column(columnDefinition = "int(1) comment '性别，1-男，2-女'")
    private Integer gender;

    @Column(unique = true,columnDefinition = "varchar(11) not null comment '手机号码'")
    private String phone;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "date comment '生日'")
    private Date birthday;

    @Column(columnDefinition = "int(3) comment '年龄'")
    private Integer age;

    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH})
    @JoinColumn(name = "dep_id",columnDefinition = "varchar(32) comment '部门ID：每个员工只能属于一个部门'")
    private Department department;

    @Column(insertable = false,columnDefinition = "int default 1 comment '状态：1-启用，0-停用，默认启用'")
    private Integer status;

    /*用户-员工关联关系的维护端，当删除employees数据时，会级联删除users中的数据*/
    @OneToOne(cascade = CascadeType.ALL,optional = false)
    /*employee表中的code字段，参考users表中的username字段*/
    @JoinColumn(name = "code",unique = true,updatable = false,referencedColumnName = "username",
            columnDefinition = "varchar(11) not null comment '用户编号：手机号码'")
    private Users user;
}
