package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * 菜单/权限信息
 */
@Entity
@Data
@Table(name = "sys_menu")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Menu extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(unique = true,columnDefinition = "int not null comment '菜单编号'")
    private Integer code;

    @Column(columnDefinition = "varchar(20) not null comment '菜单名称'")
    private String name;

    @Column(name = "parent_id",columnDefinition = "int(3) comment '上级菜单编号'")
    private Integer parentId;

    @Column(columnDefinition = "varchar(120) comment '资源路径'")
    private String url;

    /*角色-菜单关联关系：关系被维护端*/
    @ManyToMany(mappedBy = "menuList")
    private List<Role> roleList;
}
