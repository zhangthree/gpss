package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "base_price")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Price extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH},optional = false)
    @JoinColumn(name = "product_id",columnDefinition = "varchar(32) not null comment '商品ID'")
    /*所属的商品*/
    private Product product;

    /*关联关系的维护端，当删除价格信息时，与之关联的供应商也会被删除*/
    @OneToOne(cascade = CascadeType.ALL)
    /*price中的supplier_id字段参考supplier表中的id字段*/
    @JoinColumn(name = "supplier_id",referencedColumnName = "id",columnDefinition = "varchar(32) comment '供应商ID'")
    private Supplier supplier;

    @Column(name = "batch_no",columnDefinition = "varchar(20) not null comment '批号'")
    private String batchNo;

    @Column(name = "purchase_price",columnDefinition = "double(10,2) comment '采购价'")
    private Double purchasePrice;

    @Column(name = "sales_price",columnDefinition = "double(10,2) comment '销售价'")
    private Double salesPrice;

    @Column(insertable = false,columnDefinition = "int(1) default 1 comment '状态：1-启用，0-停用，默认启用'")
    private Integer status;

    @Column(columnDefinition = "varchar(11) comment '定价人'")
    private String rater;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "price_date",columnDefinition = "datetime on update current_timestamp comment '定价时间'")
    private Date priceDate;


}
