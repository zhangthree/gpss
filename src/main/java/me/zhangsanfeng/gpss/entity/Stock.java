package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "biz_stock")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Stock extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(name = "product_code",columnDefinition = "int comment '商品编码'")
    private Integer productCode;

    @Column(name = "product_name",columnDefinition = "varchar(50) comment '商品名称'")
    private String productName;

    @Column(name = "warehouse_code",columnDefinition = "int(3) comment '仓库编号'")
    private Integer warehouseCode;

    @Column(name = "warehouse_name",columnDefinition = "varchar(100) comment '仓库名称'")
    private String warehouseName;

    @Column(name = "purchase_price",columnDefinition = "double(10,2) comment '采购价'")
    private Double purchasePrice;

    @Column(name = "sales_price",columnDefinition = "double(10,2) comment '销售价'")
    private Double salesPrice;

    @Column(columnDefinition = "int default 0 comment '数量'")
    private Integer number;

    @Column(name = "batch_no",columnDefinition = "varchar(20) comment '批号' ")
    private String batchNo;

    @Column(name = "supplier_code",columnDefinition = "int comment '供应商编码'")
    private Integer supplierCode;

    @Column(name = "supplier_name",columnDefinition = "varchar(100) comment '供应商名称'")
    private String supplierName;

    @Column(insertable = false,name = "delete_flag",
            columnDefinition = "int(1) default 1 comment '删除标记：0-删除，1-未删除'")
    private Integer deleteFlag;

    /*商品<->仓库-库存关联关系：关系被维护端*/
    @OneToOne(mappedBy = "stock",cascade = {CascadeType.MERGE,CascadeType.REFRESH},optional = false)
    private ProductWarehouseRelation relation;
}
