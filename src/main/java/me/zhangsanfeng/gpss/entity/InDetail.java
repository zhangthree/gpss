package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "biz_in_detail")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class InDetail extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(name = "row_no",columnDefinition = "int(3) comment '序号'")
    private Integer rowNo;

    /*入库单据主细表关联关系：关系维护端，参考字段为主表(inOrder)的order_no*/
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH},optional = false)
    @JoinColumn(name = "order_no",referencedColumnName = "order_no",
            columnDefinition = "varchar(15) comment '入库单据号'")
    private InOrder inOrder;

    @Column(name = "product_code",
            columnDefinition = "int comment '商品编码'")
    private Integer productCode;

    @Column(name = "product_name",
            columnDefinition = "varchar(50) comment '商品名称'")
    private String productName;

    @Column(name = "product_warehouse_id",
            columnDefinition = "varchar(32) not null comment '商品-仓库关联ID'")
    private String relProWarId;

    @Column(columnDefinition = "int not null default 0 comment '数量'")
    private Integer number;

    @Column(columnDefinition = "varchar(10) comment '单位'")
    private String unit;

    @Column(columnDefinition = "double(10,2) comment '价格'")
    private Double price;

    @Column(columnDefinition = "double(10,2) comment '金额'")
    private Double amount;

    @Column(name = "batch_no",
            columnDefinition = "varchar(20) comment '批号' ")
    private String batchNo;

    @Column(name = "supplier_code",
            columnDefinition = "int comment '供应商编码'")
    private Integer supplierCode;

    @Column(name = "supplier_name",
            columnDefinition = "varchar(100) comment '供应商名称'")
    private String supplierName;
}
