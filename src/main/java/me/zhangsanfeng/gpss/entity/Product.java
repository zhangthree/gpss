package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "base_product")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Product extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(unique = true,columnDefinition = "int not null comment '编码'")
    private Integer code;

    @Column(columnDefinition = "varchar(50) not null comment '名称'")
    private String name;

    @Column(insertable = false,
            columnDefinition = "int(1) default 1 comment '启停用状态：1-启用，0-停用，默认启用'")
    private Integer status;

    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH})
    @JoinColumn(name = "type_id",columnDefinition = "varchar(32) comment '商品类别ID'")
    private Type type;

    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH})
    @JoinColumn(name = "brand_id",columnDefinition = "varchar(32) comment '商品品牌ID'")
    private Brand brand;

    @Column(name = "min_unit",columnDefinition = "varchar(10) comment '最小单位'")
    private String minUnit;

    @Column(name = "max_unit",columnDefinition = "varchar(10) comment '最大单位'")
    private String maxUnit;

    @Column(name = "unit_ratio",columnDefinition = "int(5) comment '单位比率'")
    private Integer unitRatio;

    /*商品-价格关联关系：被维护端，当删除商品时，关联的价格也将被删除*/
    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Price> priceList;

    /*商品-库存关联关系：维护端
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "stock_id",referencedColumnName = "id",
            columnDefinition = "varchar(32) comment '库存ID'")
    private Stock stock;
    */

    /*商品-仓库关联关系：关系维护端，删除product会级联删除关联表中warehouse对应的记录*/
    @ManyToMany
    @JoinTable(name = "rel_product_warehouse",
            joinColumns = @JoinColumn(name = "product_id",
                    columnDefinition = "varchar(32) not null comment '商品ID'"),
            inverseJoinColumns = @JoinColumn(name = "warehouse_id",
                    columnDefinition = "varchar(32) not null comment '仓库ID'"))
    private List<Warehouse> warehouseList;

}
