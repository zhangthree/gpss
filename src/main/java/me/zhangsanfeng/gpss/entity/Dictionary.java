package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "sys_dictionary")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class Dictionary extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(unique = true,columnDefinition = "int not null comment '编号'")
    private Integer code;

    @Column(columnDefinition = "varchar(20) not null comment '字典名称'")
    private String name;

    @Column(columnDefinition = "varchar(50) comment '字典取值'")
    private String value;

    @Column(name = "reference_table",columnDefinition = "varchar(50) comment '关联表'")
    private String referenceTable;

    @Column(name = "reference_column",columnDefinition = "varchar(40) comment '关联表字段'")
    private String referenceColumn;

}
