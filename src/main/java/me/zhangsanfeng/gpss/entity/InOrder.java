package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zhangsanfeng.gpss.common.entity.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "biz_in_order")
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class InOrder extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(unique = true,name = "order_no",
            columnDefinition = "varchar(15) not null comment '订单号'")
    private String orderNo;

    @Column(name = "warehouse_code",
            columnDefinition = "int(3) not null comment '仓库编号'")
    private Integer warehouseCode;

    @Column(name = "warehouse_name",
            columnDefinition = "varchar(100) comment '仓库名称'")
    private String warehouseName;

    @Column(columnDefinition = "int(2) not null comment '单据类别：从字典中取值'")
    private Integer type;

    @Column(insertable = false,
            columnDefinition = "int(2) default 0 comment '单据状态：从字典中取值'")
    private Integer status;

    @Column(name = "order_total",
            columnDefinition = "double(10,2) comment '单据总额'")
    private Double orderTotal;

    @Column(name = "order_owner",
            columnDefinition = "varchar(11) comment '填单人'")
    private String orderOwner;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "owner_date",
            columnDefinition = "date comment '填单时间'")
    private Date ownerDate;

    @Column(name = "order_auditor",
            columnDefinition = "varchar(11) comment '单据审核人'")
    private String orderAuditor;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "auditor_date",
            columnDefinition = "date comment '审核时间'")
    private Date auditorDate;

    @Column(columnDefinition = "varchar(100) comment '备注'")
    private String remarks;

    @Column(insertable = false,name = "delete_flag",
            columnDefinition = "int(1) default 1 comment '删除标记：0-删除，1-未删除'")
    private Integer deleteFlag;

    /*入库单据主细表关联关系：关系被维护端，删除主表记录时，级联删除细表关联数据*/
    @OneToMany(mappedBy = "inOrder",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<InDetail> inDetailList;
}
