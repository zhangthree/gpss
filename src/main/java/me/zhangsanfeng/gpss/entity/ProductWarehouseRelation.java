package me.zhangsanfeng.gpss.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "rel_product_warehouse")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class ProductWarehouseRelation implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(columnDefinition = "varchar(32) comment '唯一标识'")
    private String id;

    @Column(name = "product_id",
            columnDefinition = "varchar(32) not null comment '商品ID'")
    private String productId;

    @Column(name = "warehouse_id",
            columnDefinition = "varchar(32) not null comment '仓库ID'")
    private String warehouseId;

    /*商品<->仓库-库存关联关系：关系维护端，删除rel_product_warehouse表中的记录，将级联删除stock表中对应的数据*/
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "stock_id",referencedColumnName = "id")
    private Stock stock;
}
