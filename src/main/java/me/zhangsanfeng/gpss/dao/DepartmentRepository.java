package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DepartmentRepository extends JpaRepository<Department,String>, JpaSpecificationExecutor<Department> {
}
