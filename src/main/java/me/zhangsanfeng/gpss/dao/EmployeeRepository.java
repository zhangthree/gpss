package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EmployeeRepository extends JpaRepository<Employee,String>, JpaSpecificationExecutor<Employee> {
    public Employee findByPhone(String phone);
}
