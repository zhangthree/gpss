package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface StockRepository extends JpaRepository<Stock,String>, JpaSpecificationExecutor<Stock> {
}
