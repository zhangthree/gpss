package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UsersRepository extends JpaRepository<Users,String>, JpaSpecificationExecutor<Users> {
    Users findByUsername(String username);
}
