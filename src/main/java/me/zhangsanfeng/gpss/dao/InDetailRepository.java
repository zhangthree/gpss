package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.InDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface InDetailRepository extends JpaRepository<InDetail,String>, JpaSpecificationExecutor<InDetail> {
}
