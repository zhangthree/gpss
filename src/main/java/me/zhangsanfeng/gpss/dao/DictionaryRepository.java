package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.Dictionary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface DictionaryRepository extends JpaRepository<Dictionary,String>, JpaSpecificationExecutor<Dictionary> {
    List<Dictionary> findByReferenceTableAndReferenceColumn(String table,String column);
}