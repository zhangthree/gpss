package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BrandRepository extends JpaRepository<Brand,String>, JpaSpecificationExecutor<Brand> {
}
