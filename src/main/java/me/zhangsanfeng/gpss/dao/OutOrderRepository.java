package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.OutOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OutOrderRepository extends JpaRepository<OutOrder,String>, JpaSpecificationExecutor<OutOrder> {
}
