package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.OutDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OutDetailRepository extends JpaRepository<OutDetail,String>, JpaSpecificationExecutor<OutDetail> {
}
