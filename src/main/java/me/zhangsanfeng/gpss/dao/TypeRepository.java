package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TypeRepository extends JpaRepository<Type,String>, JpaSpecificationExecutor<Type> {
}
