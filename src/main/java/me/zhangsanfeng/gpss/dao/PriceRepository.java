package me.zhangsanfeng.gpss.dao;

import me.zhangsanfeng.gpss.entity.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PriceRepository extends JpaRepository<Price,String>, JpaSpecificationExecutor<Price> {
}
