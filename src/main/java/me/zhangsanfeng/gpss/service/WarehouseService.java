package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Warehouse;
import org.springframework.data.domain.Page;

import java.util.List;

public interface WarehouseService {

    Warehouse insert(Warehouse warehouse);

    List<Warehouse> insertBatch(List<Warehouse> warehouses);

    void delete(Warehouse warehouse);

    void deleteBatch(List<Warehouse> warehouses);

    Warehouse update(Warehouse warehouse);

    List<Warehouse> updateBatch(List<Warehouse> warehouses);

    Warehouse selectOne(Warehouse warehouse);

    List<Warehouse> selectList(Warehouse warehouse);

    Page<Warehouse> selectPage(Warehouse warehouse);
}
