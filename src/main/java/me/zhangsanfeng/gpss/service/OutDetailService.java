package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.OutDetail;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OutDetailService {

    OutDetail insert(OutDetail outDetail);

    List<OutDetail> insertBatch(List<OutDetail> outDetails);

    void delete(OutDetail outDetail);

    void deleteBatch(List<OutDetail> outDetails);

    OutDetail update(OutDetail outDetail);

    List<OutDetail> updateBatch(List<OutDetail> outDetails);

    OutDetail selectOne(OutDetail outDetail);

    List<OutDetail> selectList(OutDetail outDetail);

    Page<OutDetail> selectPage(OutDetail outDetail);
}
