package me.zhangsanfeng.gpss.service;

/**
 * @ClassName:commonService
 * @Description:公共service
 * @Author:ZhangYao
 * @Date:2018/12/6 22:34
 * @Version:1.0
 */
public interface CommonService {
    //获取订单号
    String getOrderNo(String type);
}
