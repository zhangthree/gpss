package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Stock;
import org.springframework.data.domain.Page;

import java.util.List;

public interface StockService {

    Stock insert(Stock stock);

    List<Stock> insertBatch(List<Stock> stocks);

    void delete(Stock stock);

    void deleteBatch(List<Stock> stocks);

    Stock update(Stock stock);

    List<Stock> updateBatch(List<Stock> stocks);

    Stock selectOne(Stock stock);

    List<Stock> selectList(Stock stock);

    Page<Stock> selectPage(Stock stock);
}
