package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Brand;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BrandService {

    Brand insert(Brand brand);

    List<Brand> insertBatch(List<Brand> brands);

    void delete(Brand brand);

    void deleteBatch(List<Brand> brands);

    Brand update(Brand brand);

    List<Brand> updateBatch(List<Brand> brands);

    Brand selectOne(Brand brand);

    List<Brand> selectList(Brand brand);

    Page<Brand> selectPage(Brand brand);
}
