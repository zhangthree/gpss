package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.InOrder;
import org.springframework.data.domain.Page;

import java.util.List;

public interface InOrderService {

    InOrder insert(InOrder inOrder);

    List<InOrder> insertBatch(List<InOrder> inOrders);

    void delete(InOrder inOrder);

    void deleteBatch(List<InOrder> inOrders);

    InOrder update(InOrder inOrder);

    List<InOrder> updateBatch(List<InOrder> inOrders);

    InOrder selectOne(InOrder inOrder);

    List<InOrder> selectList(InOrder inOrder);

    Page<InOrder> selectPage(InOrder inOrder);
}
