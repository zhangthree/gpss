package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Menu;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MenuService {

    Menu insert(Menu menu);

    List<Menu> insertBatch(List<Menu> menus);

    void delete(Menu menu);

    void deleteBatch(List<Menu> menus);

    Menu update(Menu menu);

    List<Menu> updateBatch(List<Menu> menus);

    Menu selectOne(Menu menu);

    List<Menu> selectList(Menu menu);

    Page<Menu> selectPage(Menu menu);
}
