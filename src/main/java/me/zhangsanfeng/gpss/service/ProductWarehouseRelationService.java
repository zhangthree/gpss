package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.ProductWarehouseRelation;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductWarehouseRelationService {

    ProductWarehouseRelation insert(ProductWarehouseRelation productWarehouseRelation);

    List<ProductWarehouseRelation> insertBatch(List<ProductWarehouseRelation> productWarehouseRelations);

    void delete(ProductWarehouseRelation productWarehouseRelation);

    void deleteBatch(List<ProductWarehouseRelation> productWarehouseRelations);

    ProductWarehouseRelation update(ProductWarehouseRelation productWarehouseRelation);

    List<ProductWarehouseRelation> updateBatch(List<ProductWarehouseRelation> productWarehouseRelations);

    ProductWarehouseRelation selectOne(ProductWarehouseRelation productWarehouseRelation);

    List<ProductWarehouseRelation> selectList(ProductWarehouseRelation productWarehouseRelation);

    Page<ProductWarehouseRelation> selectPage(ProductWarehouseRelation productWarehouseRelation);
}
