package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.InDetail;
import org.springframework.data.domain.Page;

import java.util.List;

public interface InDetailService {

    InDetail insert(InDetail inDetail);

    List<InDetail> insertBatch(List<InDetail> inDetails);

    void delete(InDetail inDetail);

    void deleteBatch(List<InDetail> inDetails);

    InDetail update(InDetail inDetail);

    List<InDetail> updateBatch(List<InDetail> inDetails);

    InDetail selectOne(InDetail inDetail);

    List<InDetail> selectList(InDetail inDetail);

    Page<InDetail> selectPage(InDetail inDetail);
}
