package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Type;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TypeService {

    Type insert(Type type);

    List<Type> insertBatch(List<Type> types);

    void delete(Type type);

    void deleteBatch(List<Type> types);

    Type update(Type type);

    List<Type> updateBatch(List<Type> types);

    Type selectOne(Type type);

    List<Type> selectList(Type type);

    Page<Type> selectPage(Type type);
}
