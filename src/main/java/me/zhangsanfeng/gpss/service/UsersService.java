package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Users;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UsersService {

    Users insert(Users user);

    List<Users> insertBatch(List<Users> users);

    void delete(Users user);

    void deleteBatch(List<Users> users);

    Users update(Users user);

    List<Users> updateBatch(List<Users> users);

    Users selectOne(Users user);

    List<Users> selectList(Users user);

    Page<Users> selectPage(Users user);
}
