package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Supplier;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SupplierService {

    Supplier insert(Supplier supplier);

    List<Supplier> insertBatch(List<Supplier> suppliers);

    void delete(Supplier supplier);

    void deleteBatch(List<Supplier> suppliers);

    Supplier update(Supplier supplier);

    List<Supplier> updateBatch(List<Supplier> suppliers);

    Supplier selectOne(Supplier supplier);

    List<Supplier> selectList(Supplier supplier);

    Page<Supplier> selectPage(Supplier supplier);
}
