package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.DepartmentRepository;
import me.zhangsanfeng.gpss.entity.Department;
import me.zhangsanfeng.gpss.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository repository;

    @Override
    public Department insert(Department department) {
        return repository.save(department);
    }

    @Override
    public List<Department> insertBatch(List<Department> departments) {
        return repository.saveAll(departments);
    }

    @Override
    public void delete(Department department) {
        repository.delete(department);
    }

    @Override
    public void deleteBatch(List<Department> departments) {
        repository.deleteAll(departments);
    }

    @Override
    public Department update(Department department) {

        UpdateUtil.copyNullProperties(repository.getOne(department.getId()),department);
        return repository.saveAndFlush(department);
    }

    @Override
    public List<Department> updateBatch(List<Department> departments) {
        for (Department department : departments) {
            UpdateUtil.copyNullProperties(repository.getOne(department.getId()),department);
        }
        return repository.saveAll(departments);
    }

    @Override
    public Department selectOne(Department department) {
        return repository.findOne(Example.of(department)).get();
    }

    @Override
    public List<Department> selectList(Department department) {
        /*排序标识*/
        Sorter sorter = department.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(department, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(department, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Department> selectPage(Department department) {
        /*排序标识*/
        Sorter sorter = department.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(department, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(department, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
