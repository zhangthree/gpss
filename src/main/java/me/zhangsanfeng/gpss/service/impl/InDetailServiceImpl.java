package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.InDetailRepository;
import me.zhangsanfeng.gpss.entity.InDetail;
import me.zhangsanfeng.gpss.service.InDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class InDetailServiceImpl implements InDetailService {

    @Autowired
    private InDetailRepository repository;

    @Override
    public InDetail insert(InDetail inDetail) {
        return repository.save(inDetail);
    }

    @Override
    public List<InDetail> insertBatch(List<InDetail> inDetails) {
        return repository.saveAll(inDetails);
    }

    @Override
    public void delete(InDetail inDetail) {
        repository.delete(inDetail);
    }

    @Override
    public void deleteBatch(List<InDetail> inDetails) {
        repository.deleteAll(inDetails);
    }

    @Override
    public InDetail update(InDetail inDetail) {
        UpdateUtil.copyNullProperties(repository.getOne(inDetail.getId()),inDetail);
        return repository.saveAndFlush(inDetail);
    }

    @Override
    public List<InDetail> updateBatch(List<InDetail> inDetails) {
        for (InDetail inDetail : inDetails) {
            UpdateUtil.copyNullProperties(repository.getOne(inDetail.getId()),inDetail);
        }
        return repository.saveAll(inDetails);
    }

    @Override
    public InDetail selectOne(InDetail inDetail) {
        return repository.findOne(Example.of(inDetail)).get();
    }

    @Override
    public List<InDetail> selectList(InDetail inDetail) {
        /*排序标识*/
        Sorter sorter = inDetail.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(inDetail, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(inDetail, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<InDetail> selectPage(InDetail inDetail) {
        /*排序标识*/
        Sorter sorter = inDetail.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(inDetail, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(inDetail, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
