package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.OutDetailRepository;
import me.zhangsanfeng.gpss.entity.OutDetail;
import me.zhangsanfeng.gpss.service.OutDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OutDetailServiceImpl implements OutDetailService {

    @Autowired
    private OutDetailRepository repository;

    @Override
    public OutDetail insert(OutDetail outDetail) {
        return repository.save(outDetail);
    }

    @Override
    public List<OutDetail> insertBatch(List<OutDetail> outDetails) {
        return repository.saveAll(outDetails);
    }

    @Override
    public void delete(OutDetail outDetail) {
        repository.delete(outDetail);
    }

    @Override
    public void deleteBatch(List<OutDetail> outDetails) {
        repository.deleteAll(outDetails);
    }

    @Override
    public OutDetail update(OutDetail outDetail) {
        UpdateUtil.copyNullProperties(repository.getOne(outDetail.getId()),outDetail);
        return repository.saveAndFlush(outDetail);
    }

    @Override
    public List<OutDetail> updateBatch(List<OutDetail> outDetails) {
        for (OutDetail outDetail : outDetails) {
            UpdateUtil.copyNullProperties(repository.getOne(outDetail.getId()),outDetail);
        }
        return repository.saveAll(outDetails);
    }

    @Override
    public OutDetail selectOne(OutDetail outDetail) {
        return repository.findOne(Example.of(outDetail)).get();
    }

    @Override
    public List<OutDetail> selectList(OutDetail outDetail) {
        /*排序标识*/
        Sorter sorter = outDetail.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(outDetail, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(outDetail, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<OutDetail> selectPage(OutDetail outDetail) {
        /*排序标识*/
        Sorter sorter = outDetail.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(outDetail, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(outDetail, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
