package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.WarehouseRepository;
import me.zhangsanfeng.gpss.entity.Warehouse;
import me.zhangsanfeng.gpss.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class WarehouseServiceImpl implements WarehouseService {

    @Autowired
    private WarehouseRepository repository;

    @Override
    public Warehouse insert(Warehouse warehouse) {
        return repository.save(warehouse);
    }

    @Override
    public List<Warehouse> insertBatch(List<Warehouse> warehouses) {
        return repository.saveAll(warehouses);
    }

    @Override
    public void delete(Warehouse warehouse) {
        repository.delete(warehouse);
    }

    @Override
    public void deleteBatch(List<Warehouse> warehouses) {
        repository.deleteAll(warehouses);
    }

    @Override
    public Warehouse update(Warehouse warehouse) {
        UpdateUtil.copyNullProperties(repository.getOne(warehouse.getId()),warehouse);
        return repository.saveAndFlush(warehouse);
    }

    @Override
    public List<Warehouse> updateBatch(List<Warehouse> warehouses) {
        for (Warehouse warehouse : warehouses) {
            UpdateUtil.copyNullProperties(repository.getOne(warehouse.getId()),warehouse);
        }
        return repository.saveAll(warehouses);
    }

    @Override
    public Warehouse selectOne(Warehouse warehouse) {
        return repository.findOne(Example.of(warehouse)).get();
    }

    @Override
    public List<Warehouse> selectList(Warehouse warehouse) {
        /*排序标识*/
        Sorter sorter = warehouse.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(warehouse, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(warehouse, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Warehouse> selectPage(Warehouse warehouse) {
        /*排序标识*/
        Sorter sorter = warehouse.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(warehouse, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(warehouse, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
