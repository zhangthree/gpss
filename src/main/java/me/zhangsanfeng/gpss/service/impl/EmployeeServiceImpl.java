package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.DataUtil;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.EmployeeRepository;
import me.zhangsanfeng.gpss.entity.Employee;
import me.zhangsanfeng.gpss.entity.Users;
import me.zhangsanfeng.gpss.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Employee insert(Employee employee) {
        Users user = employee.getUser();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        employee.setUser(user);
        return repository.save(employee);
    }

    @Override
    public List<Employee> insertBatch(List<Employee> employees){
        for (Employee employee : employees) {
            Users user = employee.getUser();
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            employee.setUser(user);
        }
        return repository.saveAll(employees);
    }

    @Override
    public void delete(Employee employee) {
        repository.delete(employee);
    }

    @Override
    public void deleteBatch(List<Employee> employees){
        repository.deleteAll(employees);
    }

    @Override
    public Employee update(Employee employee) {
        Users user = employee.getUser();
        if (DataUtil.isNotEmpty(user)){
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            employee.setUser(user);
        }
        Employee one = repository.getOne(employee.getId());
        UpdateUtil.copyNullProperties(one,employee);
        return repository.saveAndFlush(employee);
    }

    @Override
    public List<Employee> updateBatch(List<Employee> employees){
        for (Employee employee : employees) {
            Users user = employee.getUser();
            if (DataUtil.isNotEmpty(user)){
                user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
                employee.setUser(user);
            }
            Employee one = repository.getOne(employee.getId());
            UpdateUtil.copyNullProperties(one,employee);
        }
        return repository.saveAll(employees);
    }

    @Override
    public Employee selectOne(Employee employee) {
        return repository.findOne(Example.of(employee)).get();
    }

    @Override
    public List<Employee> selectList(Employee employee) {
        /*排序标识*/
        Sorter sorter = employee.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(employee, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(employee, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Employee> selectPage(Employee employee){
        /*排序标识*/
        Sorter sorter = employee.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(employee, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(employee, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);

    }
}
