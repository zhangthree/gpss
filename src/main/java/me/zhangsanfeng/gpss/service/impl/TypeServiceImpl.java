package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.TypeRepository;
import me.zhangsanfeng.gpss.entity.Type;
import me.zhangsanfeng.gpss.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeRepository repository;

    @Override
    public Type insert(Type type) {
        return repository.save(type);
    }

    @Override
    public List<Type> insertBatch(List<Type> types) {
        return repository.saveAll(types);
    }

    @Override
    public void delete(Type type) {
        repository.delete(type);
    }

    @Override
    public void deleteBatch(List<Type> types) {
        repository.deleteAll(types);
    }

    @Override
    public Type update(Type type) {
        UpdateUtil.copyNullProperties(repository.getOne(type.getId()),type);
        return repository.saveAndFlush(type);
    }

    @Override
    public List<Type> updateBatch(List<Type> types) {
        for (Type type : types) {
            UpdateUtil.copyNullProperties(repository.getOne(type.getId()),type);
        }
        return repository.saveAll(types);
    }

    @Override
    public Type selectOne(Type type) {
        return repository.findOne(Example.of(type)).get();
    }

    @Override
    public List<Type> selectList(Type type) {
        /*排序标识*/
        Sorter sorter = type.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(type, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(type, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Type> selectPage(Type type) {
        /*排序标识*/
        Sorter sorter = type.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(type, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(type, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
