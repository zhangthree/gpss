package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.CustomerRepository;
import me.zhangsanfeng.gpss.entity.Customer;
import me.zhangsanfeng.gpss.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository repository;

    @Override
    public Customer insert(Customer customer) {
        return repository.save(customer);
    }

    @Override
    public List<Customer> insertBatch(List<Customer> customers) {
        return repository.saveAll(customers);
    }

    @Override
    public void delete(Customer customer) {
        repository.delete(customer);
    }

    @Override
    public void deleteBatch(List<Customer> customers) {
        repository.deleteAll(customers);
    }

    @Override
    public Customer update(Customer customer) {

        UpdateUtil.copyNullProperties(repository.getOne(customer.getId()),customer);
        return repository.saveAndFlush(customer);
    }

    @Override
    public List<Customer> updateBatch(List<Customer> customers) {
        for (Customer customer : customers) {
            UpdateUtil.copyNullProperties(repository.getOne(customer.getId()),customer);
        }
        return repository.saveAll(customers);
    }

    @Override
    public Customer selectOne(Customer customer) {
        return repository.findOne(Example.of(customer)).get();
    }

    @Override
    public List<Customer> selectList(Customer customer) {
        /*排序标识*/
        Sorter sorter = customer.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(customer, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(customer, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Customer> selectPage(Customer customer) {
        /*排序标识*/
        Sorter sorter = customer.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(customer, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(customer, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
