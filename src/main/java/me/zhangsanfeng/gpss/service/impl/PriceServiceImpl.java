package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.PriceRepository;
import me.zhangsanfeng.gpss.entity.Price;
import me.zhangsanfeng.gpss.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class PriceServiceImpl implements PriceService {

    @Autowired
    private PriceRepository repository;

    @Override
    public Price insert(Price price) {
        return repository.save(price);
    }

    @Override
    public List<Price> insertBatch(List<Price> prices) {
        return repository.saveAll(prices);
    }

    @Override
    public void delete(Price price) {
        repository.delete(price);
    }

    @Override
    public void deleteBatch(List<Price> prices) {
        repository.deleteAll(prices);
    }

    @Override
    public Price update(Price price) {
        UpdateUtil.copyNullProperties(repository.getOne(price.getId()),price);
        return repository.saveAndFlush(price);
    }

    @Override
    public List<Price> updateBatch(List<Price> prices) {
        for (Price price : prices) {
            UpdateUtil.copyNullProperties(repository.getOne(price.getId()),price);
        }
        return repository.saveAll(prices);
    }

    @Override
    public Price selectOne(Price price) {
        return repository.findOne(Example.of(price)).get();
    }

    @Override
    public List<Price> selectList(Price price) {
        /*排序标识*/
        Sorter sorter = price.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(price, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(price, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Price> selectPage(Price price) {
        /*排序标识*/
        Sorter sorter = price.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(price, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(price, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
