package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.MenuRepository;
import me.zhangsanfeng.gpss.entity.Menu;
import me.zhangsanfeng.gpss.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepository repository;

    @Override
    public Menu insert(Menu menu) {
        return repository.save(menu);
    }

    @Override
    public List<Menu> insertBatch(List<Menu> menus) {
        return repository.saveAll(menus);
    }

    @Override
    public void delete(Menu menu) {
        repository.delete(menu);
    }

    @Override
    public void deleteBatch(List<Menu> menus) {
        repository.deleteAll(menus);
    }

    @Override
    public Menu update(Menu menu) {
        UpdateUtil.copyNullProperties(repository.getOne(menu.getId()),menu);
        return repository.saveAndFlush(menu);
    }

    @Override
    public List<Menu> updateBatch(List<Menu> menus) {
        for (Menu menu : menus) {
            UpdateUtil.copyNullProperties(repository.getOne(menu.getId()),menu);
        }
        return repository.saveAll(menus);
    }

    @Override
    public Menu selectOne(Menu menu) {
        return repository.findOne(Example.of(menu)).get();
    }

    @Override
    public List<Menu> selectList(Menu menu) {
        /*排序标识*/
        Sorter sorter = menu.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(menu, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(menu, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Menu> selectPage(Menu menu) {
        /*排序标识*/
        Sorter sorter = menu.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(menu, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(menu, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
