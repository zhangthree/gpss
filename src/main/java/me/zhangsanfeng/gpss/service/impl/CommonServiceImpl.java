package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.utils.DateUtil;
import me.zhangsanfeng.gpss.service.CommonService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @ClassName:CommonServiceImpl
 * @Description: 公共service实现类
 * @Author:ZhangYao
 * @Date:2018/12/6 22:37
 * @Version:1.0
 */
@Service
public class CommonServiceImpl implements CommonService {

    /**
     * @Author ZhangYao
     * @Description //根据类型生成订单编号
     * @Date 22:40 2018/12/6
     * @Param type:单价类型，2位长度，入库-rk、出库-ck等等
     * @return java.lang.String
     **/
    @Override
    public String getOrderNo(String type) {
        String currentDate = DateUtil.date2String(new Date(), DateUtil.DATE_PATTERN.YYYYMMDDHHMMSS);
        String randomNumeric = RandomStringUtils.randomNumeric(4);
        return type + currentDate + randomNumeric;
    }
}
