package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.OutOrderRepository;
import me.zhangsanfeng.gpss.entity.OutOrder;
import me.zhangsanfeng.gpss.service.OutOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OutOrderServiceImpl implements OutOrderService {

    @Autowired
    private OutOrderRepository repository;

    @Override
    public OutOrder insert(OutOrder outOrder) {
        return repository.save(outOrder);
    }

    @Override
    public List<OutOrder> insertBatch(List<OutOrder> outOrders) {
        return repository.saveAll(outOrders);
    }

    @Override
    public void delete(OutOrder outOrder) {
        repository.delete(outOrder);
    }

    @Override
    public void deleteBatch(List<OutOrder> outOrders) {
        repository.deleteAll(outOrders);
    }

    @Override
    public OutOrder update(OutOrder outOrder) {
        UpdateUtil.copyNullProperties(repository.getOne(outOrder.getId()),outOrder);
        return repository.saveAndFlush(outOrder);
    }

    @Override
    public List<OutOrder> updateBatch(List<OutOrder> outOrders) {
        for (OutOrder outOrder : outOrders) {
            UpdateUtil.copyNullProperties(repository.getOne(outOrder.getId()),outOrder);
        }
        return repository.saveAll(outOrders);
    }

    @Override
    public OutOrder selectOne(OutOrder outOrder) {
        return repository.findOne(Example.of(outOrder)).get();
    }

    @Override
    public List<OutOrder> selectList(OutOrder outOrder) {
        /*排序标识*/
        Sorter sorter = outOrder.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(outOrder, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(outOrder, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<OutOrder> selectPage(OutOrder outOrder) {
        /*排序标识*/
        Sorter sorter = outOrder.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(outOrder, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(outOrder, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
