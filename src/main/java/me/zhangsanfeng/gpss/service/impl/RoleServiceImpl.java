package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.RoleRepository;
import me.zhangsanfeng.gpss.entity.Role;
import me.zhangsanfeng.gpss.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository repository;

    @Override
    public Role insert(Role role) {
        return repository.save(role);
    }

    @Override
    public List<Role> insertBatch(List<Role> roles) {
        return repository.saveAll(roles);
    }

    @Override
    public void delete(Role role) {
        repository.delete(role);
    }

    @Override
    public void deleteBatch(List<Role> roles) {
        repository.deleteAll(roles);
    }

    @Override
    public Role update(Role role) {
        UpdateUtil.copyNullProperties(repository.getOne(role.getId()),role);
        return repository.saveAndFlush(role);
    }

    @Override
    public List<Role> updateBatch(List<Role> roles) {
        for (Role role : roles) {
            UpdateUtil.copyNullProperties(repository.getOne(role.getId()),role);
        }
        return repository.saveAll(roles);
    }

    @Override
    public Role selectOne(Role role) {
        return repository.findOne(Example.of(role)).get();
    }

    @Override
    public List<Role> selectList(Role role) {
        /*排序标识*/
        Sorter sorter = role.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(role, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(role, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Role> selectPage(Role role) {
        /*排序标识*/
        Sorter sorter = role.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(role, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(role, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
