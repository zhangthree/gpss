package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.SupplierRepository;
import me.zhangsanfeng.gpss.entity.Supplier;
import me.zhangsanfeng.gpss.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepository repository;

    @Override
    public Supplier insert(Supplier supplier) {
        return repository.save(supplier);
    }

    @Override
    public List<Supplier> insertBatch(List<Supplier> suppliers) {
        return repository.saveAll(suppliers);
    }

    @Override
    public void delete(Supplier supplier) {
        repository.delete(supplier);
    }

    @Override
    public void deleteBatch(List<Supplier> suppliers) {
        repository.deleteAll(suppliers);
    }

    @Override
    public Supplier update(Supplier supplier) {
        UpdateUtil.copyNullProperties(repository.getOne(supplier.getId()),supplier);
        return repository.saveAndFlush(supplier);
    }

    @Override
    public List<Supplier> updateBatch(List<Supplier> suppliers) {
        for (Supplier supplier : suppliers) {
            UpdateUtil.copyNullProperties(repository.getOne(supplier.getId()),supplier);
        }
        return repository.saveAll(suppliers);
    }

    @Override
    public Supplier selectOne(Supplier supplier) {
        return repository.findOne(Example.of(supplier)).get();
    }

    @Override
    public List<Supplier> selectList(Supplier supplier) {
        /*排序标识*/
        Sorter sorter = supplier.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(supplier, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(supplier, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Supplier> selectPage(Supplier supplier) {
        /*排序标识*/
        Sorter sorter = supplier.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(supplier, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(supplier, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
