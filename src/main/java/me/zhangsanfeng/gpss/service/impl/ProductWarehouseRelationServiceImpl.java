package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.ProductWarehouseRelationRepository;
import me.zhangsanfeng.gpss.entity.ProductWarehouseRelation;
import me.zhangsanfeng.gpss.service.ProductWarehouseRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductWarehouseRelationServiceImpl implements ProductWarehouseRelationService {

    @Autowired
    private ProductWarehouseRelationRepository repository;

    @Override
    public ProductWarehouseRelation insert(ProductWarehouseRelation productWarehouseRelation) {
        return repository.save(productWarehouseRelation);
    }

    @Override
    public List<ProductWarehouseRelation> insertBatch(List<ProductWarehouseRelation> productWarehouseRelations) {
        return repository.saveAll(productWarehouseRelations);
    }

    @Override
    public void delete(ProductWarehouseRelation productWarehouseRelation) {
        repository.delete(productWarehouseRelation);
    }

    @Override
    public void deleteBatch(List<ProductWarehouseRelation> productWarehouseRelations) {
        repository.deleteAll(productWarehouseRelations);
    }

    @Override
    public ProductWarehouseRelation update(ProductWarehouseRelation productWarehouseRelation) {
        UpdateUtil.copyNullProperties(repository.getOne(productWarehouseRelation.getId()),productWarehouseRelation);
        return repository.saveAndFlush(productWarehouseRelation);
    }

    @Override
    public List<ProductWarehouseRelation> updateBatch(List<ProductWarehouseRelation> productWarehouseRelations) {
        for (ProductWarehouseRelation productWarehouseRelation : productWarehouseRelations) {
            UpdateUtil.copyNullProperties(repository.getOne(productWarehouseRelation.getId()),productWarehouseRelation);
        }
        return repository.saveAll(productWarehouseRelations);
    }

    @Override
    public ProductWarehouseRelation selectOne(ProductWarehouseRelation productWarehouseRelation) {
        return repository.findOne(Example.of(productWarehouseRelation)).get();
    }

    @Override
    public List<ProductWarehouseRelation> selectList(ProductWarehouseRelation productWarehouseRelation) {
        Example<ProductWarehouseRelation> example = Example.of(productWarehouseRelation, ExampleMatcher.matchingAll());
        return repository.findAll(example);
    }

    @Override
    public Page<ProductWarehouseRelation> selectPage(ProductWarehouseRelation productWarehouseRelation) {
        Example<ProductWarehouseRelation> example = Example.of(productWarehouseRelation, ExampleMatcher.matchingAll());
        Pageable page = PageRequest.of(0, 1000);
        return repository.findAll(example,page);
    }
}
