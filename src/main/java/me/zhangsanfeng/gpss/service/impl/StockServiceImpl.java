package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.StockRepository;
import me.zhangsanfeng.gpss.entity.Stock;
import me.zhangsanfeng.gpss.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository repository;

    @Override
    public Stock insert(Stock stock) {
        return repository.save(stock);
    }

    @Override
    public List<Stock> insertBatch(List<Stock> stocks) {
        return repository.saveAll(stocks);
    }

    @Override
    public void delete(Stock stock) {
        repository.delete(stock);
    }

    @Override
    public void deleteBatch(List<Stock> stocks) {
        repository.deleteAll(stocks);
    }

    @Override
    public Stock update(Stock stock) {
        UpdateUtil.copyNullProperties(repository.getOne(stock.getId()),stock);
        return repository.saveAndFlush(stock);
    }

    @Override
    public List<Stock> updateBatch(List<Stock> stocks) {
        for (Stock stock : stocks) {
            UpdateUtil.copyNullProperties(repository.getOne(stock.getId()),stock);
        }
        return repository.saveAll(stocks);
    }

    @Override
    public Stock selectOne(Stock stock) {
        return repository.findOne(Example.of(stock)).get();
    }

    @Override
    public List<Stock> selectList(Stock stock) {
        /*排序标识*/
        Sorter sorter = stock.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(stock, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(stock, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Stock> selectPage(Stock stock) {
        /*排序标识*/
        Sorter sorter = stock.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(stock, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(stock, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }
}
