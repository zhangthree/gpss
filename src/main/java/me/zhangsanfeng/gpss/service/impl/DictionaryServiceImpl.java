package me.zhangsanfeng.gpss.service.impl;

import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.DictionaryRepository;
import me.zhangsanfeng.gpss.entity.Dictionary;
import me.zhangsanfeng.gpss.service.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class DictionaryServiceImpl implements DictionaryService {

    @Autowired
    private DictionaryRepository repository;

    @Override
    public Dictionary insert(Dictionary dictionary) {
        return repository.save(dictionary);
    }

    @Override
    public List<Dictionary> insertBatch(List<Dictionary> dictionaries) {
        return repository.saveAll(dictionaries);
    }

    @Override
    public void delete(Dictionary dictionary) {
        repository.delete(dictionary);
    }

    @Override
    public void deleteBatch(List<Dictionary> dictionaries) {
        repository.deleteAll(dictionaries);
    }

    @Override
    public Dictionary update(Dictionary dictionary) {
        UpdateUtil.copyNullProperties(repository.getOne(dictionary.getId()),dictionary);
        return repository.saveAndFlush(dictionary);
    }

    @Override
    public List<Dictionary> updateBatch(List<Dictionary> dictionaries) {
        for (Dictionary dictionary : dictionaries) {
            UpdateUtil.copyNullProperties(repository.getOne(dictionary.getId()),dictionary);
        }
        return repository.saveAll(dictionaries);
    }

    @Override
    public Dictionary selectOne(Dictionary dictionary) {
        return repository.findOne(Example.of(dictionary)).get();
    }

    @Override
    public List<Dictionary> selectList(Dictionary dictionary) {
        /*排序标识*/
        Sorter sorter = dictionary.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(dictionary, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(dictionary, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Dictionary> selectPage(Dictionary dictionary) {
        /*排序标识*/
        Sorter sorter = dictionary.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(dictionary, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(dictionary, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }

    @Override
    public Map<String, Map<String,Object>> selectMap(Dictionary dictionary) {
        String table = dictionary.getReferenceTable();
        String column = dictionary.getReferenceColumn();
        List<Dictionary> dictionaryList = repository.findByReferenceTableAndReferenceColumn(table, column);
        Map<String,Object> kvMap = new HashMap<>(dictionaryList.size());
        for (Dictionary dictionaryResult : dictionaryList) {
            kvMap.put(dictionaryResult.getName(),dictionaryResult.getValue());
        }
        Map<String,Map<String,Object>> resMap = new HashMap<>(kvMap.size());
        resMap.put(table+column,kvMap);
        return resMap;
    }
}
