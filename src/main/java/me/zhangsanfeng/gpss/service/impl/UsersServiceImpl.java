package me.zhangsanfeng.gpss.service.impl;

import lombok.extern.slf4j.Slf4j;
import me.zhangsanfeng.gpss.common.entity.Sorter;
import me.zhangsanfeng.gpss.common.utils.DataUtil;
import me.zhangsanfeng.gpss.common.utils.QueryConditionUtil;
import me.zhangsanfeng.gpss.common.utils.UpdateUtil;
import me.zhangsanfeng.gpss.dao.UsersRepository;
import me.zhangsanfeng.gpss.entity.Users;
import me.zhangsanfeng.gpss.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class UsersServiceImpl implements UsersService, UserDetailsService {

    @Autowired
    private UsersRepository repository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Users insert(Users user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return repository.save(user);
    }

    @Override
    public List<Users> insertBatch(List<Users> users){
        for (Users user : users) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        }
        return repository.saveAll(users);
    }

    @Override
    public void delete(Users user) {
        repository.delete(user);
    }

    @Override
    public void deleteBatch(List<Users> users){
        repository.deleteAll(users);
    }

    @Override
    public Users update(Users user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Users one = repository.getOne(user.getId());
        UpdateUtil.copyNullProperties(one,user);
        return repository.saveAndFlush(user);
    }

    @Override
    public List<Users> updateBatch(List<Users> users){
        for (Users user : users) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            Users one = repository.getOne(user.getId());
            UpdateUtil.copyNullProperties(one,user);
        }
        return repository.saveAll(users);
    }

    @Override
    public Users selectOne(Users user) {
        return repository.findOne(Example.of(user)).get();
    }

    @Override
    public List<Users> selectList(Users user) {
        /*排序标识*/
        Sorter sorter = user.getSorter();

        Map<String, Object> condition;
        //不排序
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()) {
            condition = QueryConditionUtil.getWhereCondition(user, false, false);
        } else {
            condition = QueryConditionUtil.getWhereCondition(user, false, true);
        }

        return QueryConditionUtil.getQueryInstanceOfList(condition, repository);
    }

    @Override
    public Page<Users> selectPage(Users user) {
        /*排序标识*/
        Sorter sorter = user.getSorter();

        Map<String, Object> condition;
        if (null == sorter.getDesc() && null == sorter.getAsc() && null == sorter.getPrior()){
            condition = QueryConditionUtil.getWhereCondition(user, true, false);
        }else {
            condition = QueryConditionUtil.getWhereCondition(user, true, true);
        }

        return QueryConditionUtil.getQueryInstanceOfPage(condition,repository);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Users user = repository.findByUsername(s);
        if (DataUtil.isEmpty(user)){
            throw new UsernameNotFoundException("用户"+s+"不存在");
        }
        return user;
    }
}
