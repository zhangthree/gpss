package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Price;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PriceService {

    Price insert(Price price);

    List<Price> insertBatch(List<Price> prices);

    void delete(Price price);

    void deleteBatch(List<Price> prices);

    Price update(Price price);

    List<Price> updateBatch(List<Price> prices);

    Price selectOne(Price price);

    List<Price> selectList(Price price);

    Page<Price> selectPage(Price price);
}
