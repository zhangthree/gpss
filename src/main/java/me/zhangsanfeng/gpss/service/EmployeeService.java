package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Employee;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EmployeeService {

    Employee insert(Employee employee);

    List<Employee> insertBatch(List<Employee> employees);

    void delete(Employee employee);

    void deleteBatch(List<Employee> employees);

    Employee update(Employee employee);

    List<Employee> updateBatch(List<Employee> employees);

    Employee selectOne(Employee employee);

    List<Employee> selectList(Employee employee);

    Page<Employee> selectPage(Employee employee);
}
