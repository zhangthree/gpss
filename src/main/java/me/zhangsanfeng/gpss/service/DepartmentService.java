package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Department;
import org.springframework.data.domain.Page;

import java.util.List;

public interface DepartmentService {

    Department insert(Department department);

    List<Department> insertBatch(List<Department> departments);

    void delete(Department department);

    void deleteBatch(List<Department> departments);

    Department update(Department department);

    List<Department> updateBatch(List<Department> departments);

    Department selectOne(Department department);

    List<Department> selectList(Department department);

    Page<Department> selectPage(Department department);
}
