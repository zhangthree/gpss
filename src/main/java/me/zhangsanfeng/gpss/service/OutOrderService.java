package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.OutOrder;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OutOrderService {

    OutOrder insert(OutOrder outOrder);

    List<OutOrder> insertBatch(List<OutOrder> outOrders);

    void delete(OutOrder outOrder);

    void deleteBatch(List<OutOrder> outOrders);

    OutOrder update(OutOrder outOrder);

    List<OutOrder> updateBatch(List<OutOrder> outOrders);

    OutOrder selectOne(OutOrder outOrder);

    List<OutOrder> selectList(OutOrder outOrder);

    Page<OutOrder> selectPage(OutOrder outOrder);
}
