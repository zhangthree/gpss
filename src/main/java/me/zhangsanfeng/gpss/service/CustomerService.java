package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Customer;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CustomerService {

    Customer insert(Customer customer);

    List<Customer> insertBatch(List<Customer> customers);

    void delete(Customer customer);

    void deleteBatch(List<Customer> customers);

    Customer update(Customer customer);

    List<Customer> updateBatch(List<Customer> customers);

    Customer selectOne(Customer customer);

    List<Customer> selectList(Customer customer);

    Page<Customer> selectPage(Customer customer);
}
