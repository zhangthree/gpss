package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Dictionary;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface DictionaryService {

    Dictionary insert(Dictionary dictionary);

    List<Dictionary> insertBatch(List<Dictionary> dictionaries);

    void delete(Dictionary dictionary);

    void deleteBatch(List<Dictionary> dictionaries);

    Dictionary update(Dictionary dictionary);

    List<Dictionary> updateBatch(List<Dictionary> dictionaries);

    Dictionary selectOne(Dictionary dictionary);

    List<Dictionary> selectList(Dictionary dictionary);

    Page<Dictionary> selectPage(Dictionary dictionary);

    Map<String,Map<String,Object>> selectMap(Dictionary dictionary);

}
