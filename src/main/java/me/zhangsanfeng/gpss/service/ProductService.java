package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService {

    Product insert(Product product);

    List<Product> insertBatch(List<Product> products);

    void delete(Product product);

    void deleteBatch(List<Product> products);

    Product update(Product product);

    List<Product> updateBatch(List<Product> products);

    Product selectOne(Product product);

    List<Product> selectList(Product product);

    Page<Product> selectPage(Product product);
}
