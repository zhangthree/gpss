package me.zhangsanfeng.gpss.service;

import me.zhangsanfeng.gpss.entity.Role;
import org.springframework.data.domain.Page;

import java.util.List;

public interface RoleService {

    Role insert(Role role);

    List<Role> insertBatch(List<Role> roles);

    void delete(Role role);

    void deleteBatch(List<Role> roles);

    Role update(Role role);

    List<Role> updateBatch(List<Role> roles);

    Role selectOne(Role role);

    List<Role> selectList(Role role);

    Page<Role> selectPage(Role role);
}
