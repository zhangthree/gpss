package me.zhangsanfeng.gpss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class GpssApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpssApplication.class, args);
    }
}
