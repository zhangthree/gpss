package me.zhangsanfeng.gpss.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @ClassName:CorsConfig
 * @Description:Ajax跨域请求过滤器
 * @Author:ZhangYao
 * @Date:2018/12/6 21:35
 * @Version:1.0
 */

@Configuration
public class CorsConfig {
    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*"); // 任何域名
        corsConfiguration.addAllowedHeader("*"); // 任何头
        corsConfiguration.addAllowedMethod("*"); // 所有方法，post、get等
        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", buildConfig());
        return new CorsFilter(source);
    }
}
