package me.zhangsanfeng.gpss.common.config;

import me.zhangsanfeng.gpss.common.entity.JwtProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JwtPropertyBean {
    @Bean
    @ConfigurationProperties(prefix = "jwt.config")
    public JwtProperty jwtProperty(){
        return new JwtProperty();
    }
}
