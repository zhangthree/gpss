package me.zhangsanfeng.gpss.common.config;

import lombok.extern.slf4j.Slf4j;
import me.zhangsanfeng.gpss.common.entity.JwtProperty;
import me.zhangsanfeng.gpss.common.filter.JWTAuthenticationFilter;
import me.zhangsanfeng.gpss.common.filter.JWTLoginFilter;
import me.zhangsanfeng.gpss.common.handler.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @ClassName:WebSecurityConfig
 * @Description:SpringSecurity的配置,将JWTLoginFilter，JWTAuthenticationFilter两个过滤器组合在一起
 * @Author:ZhangYao
 * @Date:2018/11/5 22:51
 * @Version:1.0
 */
@Slf4j
@Configuration
@EnableWebSecurity
@Order(SecurityProperties.BASIC_AUTH_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private JwtProperty jwtProperty;

    @Autowired
    private AuthenticationEntryPointHandler pointHandler;

    @Autowired
    private LoginSuccessHandler loginSuccessHandler;

    @Autowired
    private LoginFailureHandler loginFailureHandler;

    @Autowired
    private LogoutOverHandler logoutOverHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/employee/signup")
                .permitAll()
                .antMatchers("/swagger-ui.html*/**",
                        "/swagger-resources/**",
                        "/images/**",
                        "/webjars/**",
                        "/v2/api-docs",
                        "/configuration/security",
                        "/configuration/ui")
                .permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .successHandler(loginSuccessHandler)
                .failureHandler(loginFailureHandler)
                .and()
                .logout()
                //.logoutUrl("/logout")
                .logoutSuccessHandler(logoutOverHandler)
                .and()
                .exceptionHandling().authenticationEntryPoint(pointHandler)
                .and()
                .addFilter(new JWTLoginFilter(authenticationManager(),jwtProperty))
                .addFilter(new JWTAuthenticationFilter(authenticationManager(),jwtProperty));
        //super.configure(http);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
        super.configure(auth);
    }

}
