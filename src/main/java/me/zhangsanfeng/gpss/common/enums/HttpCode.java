package me.zhangsanfeng.gpss.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Ajax 请求时的自定义查询状态码，主要参考Http状态码，但并不完全对应
 */
@Getter
@AllArgsConstructor
public enum HttpCode {
    /** -1自定义异常 */
    EX(-1,"自定义异常"),
    /** 200请求成功 */
    OK(0,"请求成功"),
    /** 207频繁操作 */
    MULTI_STATUS(207,"频繁操作"),
    /** 400请求参数出错 */
    BAD_REQUEST(400,"请求参数出错"),
    /** 401没有登录 */
    UNAUTHORIZED(401,"权限验证失败"),
    /** 403没有权限 */
    FORBIDDEN(403,"禁止访问"),
    /** 404找不到页面 */
    NOT_FOUND(404,"未知请求"),
    /** 408请求超时 */
    REQUEST_TIMEOUT(408,"请求超时"),
    /** 500服务器出错 */
    SERVER_ERROR(500,"服务器未知错误"),
    /** 999登录失败 */
    LOGIN_FAILURE(999,"登录失败");

    private final Integer code;
    private final String msg;
}
