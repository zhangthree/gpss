package me.zhangsanfeng.gpss.common.entity;

import lombok.Data;

@Data
public class JwtProperty {
    /** 过期时间*/
    private long expiration;
    /** 发布者*/
    private String issuer;
    /** 密钥key*/
    private String base64Security;
}
