package me.zhangsanfeng.gpss.common.entity;

import lombok.Data;

@Data
public class PageResult extends Result {
    /*页序号*/
    private Integer page;

    /*页容量*/
    private Integer size;

    /*总记录数*/
    private Integer pages;
}
