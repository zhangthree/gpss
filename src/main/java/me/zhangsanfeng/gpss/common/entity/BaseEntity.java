package me.zhangsanfeng.gpss.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.util.Date;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
public class BaseEntity {

    /*创建时间*/
    @CreatedDate
    @Column(name = "insert_time",columnDefinition = "datetime comment '创建时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date insertTime;

    /*创建人*/
    @CreatedBy
    @Column(name = "insert_by",columnDefinition = "varchar(15) comment '创建人'")
    private String insertBy;

    /*修改时间*/
    @LastModifiedDate
    @Column(name = "update_time",columnDefinition = "datetime comment '修改时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /*修改人*/
    @LastModifiedBy
    @Column(name = "update_by",columnDefinition = "varchar(15) comment '修改人'")
    private String updateBy;

    /*分页参数：每页数量*/
    @Transient
    private Integer pageSize;

    /*分页参数：当前第几页*/
    @Transient
    private Integer pageNum;

    /*排序参数：{"asc":"code,name"}*/
    @Transient
    private Sorter sorter = new Sorter();
}
