package me.zhangsanfeng.gpss.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName:Result
 * @Description:接口统一返回json格式
 * @Author:ZhangYao
 * @Date:2018/11/3 01:55
 * @Version:1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {

    /*转态码*/
    private Integer code;

    /*提示信息*/
    private String msg;

    /*返回值*/
    private T data;

    /*总记录数*/
    private Integer count=0;
}
