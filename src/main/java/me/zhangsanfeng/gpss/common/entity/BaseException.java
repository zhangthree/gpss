package me.zhangsanfeng.gpss.common.entity;

import lombok.Data;

/**
 * @ClassName:Exception
 * @Description:统一异常处理，自定义异常实体类
 * @Author:ZhangYao
 * @Date:2018/11/3 02:46
 * @Version:1.0
 */

@Data
public class BaseException extends RuntimeException {
    private Integer code;

    public BaseException(String msg){
        super(msg);
    }
}
