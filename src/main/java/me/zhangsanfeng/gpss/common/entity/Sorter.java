package me.zhangsanfeng.gpss.common.entity;

import lombok.Data;

/**
 * 排序字段
 */
@Data
public class Sorter {
    private String asc;
    private String desc;
    private String prior;

}
