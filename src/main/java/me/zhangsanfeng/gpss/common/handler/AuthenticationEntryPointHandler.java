package me.zhangsanfeng.gpss.common.handler;

import lombok.extern.slf4j.Slf4j;
import me.zhangsanfeng.gpss.common.utils.JsonUtil;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 未登录访问处理器
 */
@Slf4j
@Component
public class AuthenticationEntryPointHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException e) throws IOException, ServletException {
        log.info("未登录访问处理器，httpStatus={}：",response.getStatus());
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(JsonUtil.toJsonString(ResultUtil.unauthorized("请先登录再进行操作")));
        e.printStackTrace();
    }
}
