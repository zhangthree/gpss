package me.zhangsanfeng.gpss.common.handler;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import lombok.extern.slf4j.Slf4j;
import me.zhangsanfeng.gpss.common.entity.BaseException;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@ControllerAdvice
public class LocalExceptionHandler {

    @ResponseBody
    @ExceptionHandler
    public Result handle(Exception e){

        if (e instanceof BaseException){//自定义异常
            return ResultUtil.except(e);
        }else {//未知异常
            log.info("系统未知错误：",e);
            return ResultUtil.error(e);
        }
    }
}
