package me.zhangsanfeng.gpss.common.aspect;

import lombok.extern.slf4j.Slf4j;
import me.zhangsanfeng.gpss.common.utils.DataUtil;
import me.zhangsanfeng.gpss.common.utils.MapUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName:HttpAspect
 * @Description:实践案例，记录每一个http请求的日志
 * @Author:ZhangYao
 * @Date:2018/11/3 00:43
 * @Version:1.0
 */
@Aspect
@Component
@Slf4j
public class HttpLogAspect {

    @Pointcut("execution(public * me.zhangsanfeng.gpss.controller.*.*(..))")
    public void log(){

    }

    @Before("log()")
    public void doBefore(JoinPoint joinPoint){
        log.info("进入方法之前……");
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        //url
        log.info("url={}",request.getRequestURL());

        //method
        log.info("method={}",request.getMethod());

        //ip
        log.info("ip={}",request.getRemoteAddr());

        //类方法
        log.info("class_method={}",joinPoint.getSignature().getDeclaringTypeName()+"."
                +joinPoint.getSignature().getName());

        //参数
        log.info("args={}",joinPoint.getArgs());
    }

    @After("log()")
    public void doAfter(){
        log.info("离开方法之后……");
    }

}
