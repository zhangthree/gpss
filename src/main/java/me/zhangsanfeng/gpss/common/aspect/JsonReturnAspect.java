package me.zhangsanfeng.gpss.common.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class JsonReturnAspect {

    @Pointcut("execution(public * me.zhangsanfeng.gpss.controller.*.*(..))")
    public void cut(){

    }

    /**
     * 过滤返回值中的特定字段
     * @param object
     */
    @AfterReturning(pointcut = "cut()",returning = "object")
    public void doAfterReturn(Object object){

    }
}
