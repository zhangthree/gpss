package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Stock;
import me.zhangsanfeng.gpss.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("库存信息")
@RestController
@RequestMapping("/stock")
public class StockController {

    @Autowired
    private StockService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Stock stock){
        return ResultUtil.success(service.insert(stock));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Stock> stocks){
        return ResultUtil.success(service.insertBatch(stocks));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody Stock stock){
        service.delete(stock);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<Stock> stocks){
        service.deleteBatch(stocks);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Stock stock){
        return ResultUtil.success(service.update(stock));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Stock> stocks){
        return ResultUtil.success(service.updateBatch(stocks));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Stock stock){
        return ResultUtil.success(service.selectOne(stock));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Stock stock){
        return ResultUtil.success(service.selectList(stock));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Stock stock){
        return ResultUtil.success(service.selectPage(stock));
    }
}
