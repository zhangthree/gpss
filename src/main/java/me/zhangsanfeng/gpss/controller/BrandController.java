package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Brand;
import me.zhangsanfeng.gpss.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("品牌信息")
@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private BrandService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Brand brand){
        return ResultUtil.success(service.insert(brand));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Brand> brands){
        return ResultUtil.success(service.insertBatch(brands));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody Brand brand){
        service.delete(brand);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<Brand> brands){
        service.deleteBatch(brands);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Brand brand){
        return ResultUtil.success(service.update(brand));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Brand> brands){
        return ResultUtil.success(service.updateBatch(brands));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Brand brand){
        return ResultUtil.success(service.selectOne(brand));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Brand brand){
        return ResultUtil.success(service.selectList(brand));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Brand brand){
        return ResultUtil.success(service.selectPage(brand));
    }
}
