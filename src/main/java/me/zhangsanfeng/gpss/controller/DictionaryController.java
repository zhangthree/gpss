package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Dictionary;
import me.zhangsanfeng.gpss.service.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("字典信息")
@RestController
@RequestMapping("/dictionary")
public class DictionaryController {

    @Autowired
    private DictionaryService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Dictionary dictionary){
        return ResultUtil.success(service.insert(dictionary));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Dictionary> dictionaries){
        return ResultUtil.success(service.insertBatch(dictionaries));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody Dictionary dictionary){
        service.delete(dictionary);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<Dictionary> dictionaries){
        service.deleteBatch(dictionaries);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Dictionary dictionary){
        return ResultUtil.success(service.update(dictionary));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Dictionary> dictionaries){
        return ResultUtil.success(service.updateBatch(dictionaries));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Dictionary dictionary){
        return ResultUtil.success(service.selectOne(dictionary));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Dictionary dictionary){
        return ResultUtil.success(service.selectList(dictionary));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Dictionary dictionary){
        return ResultUtil.success(service.selectPage(dictionary));
    }

    @PostMapping("/map")
    @ApiOperation("查询键值对列表")
    public Result selectMap(@RequestBody Dictionary dictionary){
        return ResultUtil.success(service.selectMap(dictionary));
    }
}
