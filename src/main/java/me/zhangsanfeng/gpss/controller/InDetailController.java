package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.InDetail;
import me.zhangsanfeng.gpss.service.InDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("入库明细")
@RestController
@RequestMapping("/indetail")
public class InDetailController {

    @Autowired
    private InDetailService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody InDetail inDetail){
        return ResultUtil.success(service.insert(inDetail));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<InDetail> inDetails){
        return ResultUtil.success(service.insertBatch(inDetails));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody InDetail inDetail){
        service.delete(inDetail);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<InDetail> inDetails){
        service.deleteBatch(inDetails);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody InDetail inDetail){
        return ResultUtil.success(service.update(inDetail));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<InDetail> inDetails){
        return ResultUtil.success(service.updateBatch(inDetails));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody InDetail inDetail){
        return ResultUtil.success(service.selectOne(inDetail));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody InDetail inDetail){
        return ResultUtil.success(service.selectList(inDetail));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody InDetail inDetail){
        return ResultUtil.success(service.selectPage(inDetail));
    }
}
