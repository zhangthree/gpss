package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Employee;
import me.zhangsanfeng.gpss.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api("员工信息")
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    @PostMapping("/signup")
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Employee employee){
        return ResultUtil.success(service.insert(employee));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Employee> employees){
        return ResultUtil.success(service.insertBatch(employees));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入用户表id(users.id)，否则级联删除用户表(users)关联数据失败，产生用户表(users)垃圾数据")
    public Result delete(@RequestBody Employee employee){
        service.delete(employee);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入用户表id(users.id)，否则级联删除用户表(users)关联数据失败，产生用户表(users)垃圾数据")
    public Result deleteBatch(@RequestBody List<Employee> employees){
        service.deleteBatch(employees);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Employee employee){
        return ResultUtil.success(service.update(employee));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Employee> employees){
        return ResultUtil.success(service.updateBatch(employees));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Employee employee){
        return ResultUtil.success(service.selectOne(employee));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Employee employee){
        return ResultUtil.success(service.selectList(employee));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Employee employee){
        return ResultUtil.success(service.selectPage(employee));
    }
}
