package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.ProductWarehouseRelation;
import me.zhangsanfeng.gpss.service.ProductWarehouseRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("商品与仓库关联信息")
@RestController
@RequestMapping("/productwarehouserel")
public class ProductWarehouseRelationController {

    @Autowired
    private ProductWarehouseRelationService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody ProductWarehouseRelation productWarehouseRelation){
        return ResultUtil.success(service.insert(productWarehouseRelation));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<ProductWarehouseRelation> productWarehouseRelations){
        return ResultUtil.success(service.insertBatch(productWarehouseRelations));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody ProductWarehouseRelation productWarehouseRelation){
        service.delete(productWarehouseRelation);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<ProductWarehouseRelation> productWarehouseRelations){
        service.deleteBatch(productWarehouseRelations);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody ProductWarehouseRelation productWarehouseRelation){
        return ResultUtil.success(service.update(productWarehouseRelation));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<ProductWarehouseRelation> productWarehouseRelations){
        return ResultUtil.success(service.updateBatch(productWarehouseRelations));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody ProductWarehouseRelation productWarehouseRelation){
        return ResultUtil.success(service.selectOne(productWarehouseRelation));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody ProductWarehouseRelation productWarehouseRelation){
        return ResultUtil.success(service.selectList(productWarehouseRelation));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody ProductWarehouseRelation productWarehouseRelation){
        return ResultUtil.success(service.selectPage(productWarehouseRelation));
    }
}
