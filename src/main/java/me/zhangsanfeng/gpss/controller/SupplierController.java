package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Supplier;
import me.zhangsanfeng.gpss.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("供应商信息")
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Supplier supplier){
        return ResultUtil.success(service.insert(supplier));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Supplier> suppliers){
        return ResultUtil.success(service.insertBatch(suppliers));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody Supplier supplier){
        service.delete(supplier);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<Supplier> suppliers){
        service.deleteBatch(suppliers);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Supplier supplier){
        return ResultUtil.success(service.update(supplier));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Supplier> suppliers){
        return ResultUtil.success(service.updateBatch(suppliers));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Supplier supplier){
        return ResultUtil.success(service.selectOne(supplier));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Supplier supplier){
        return ResultUtil.success(service.selectList(supplier));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Supplier supplier){
        return ResultUtil.success(service.selectPage(supplier));
    }
}
