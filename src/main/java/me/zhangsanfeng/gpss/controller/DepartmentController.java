package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Department;
import me.zhangsanfeng.gpss.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("部门信息")
@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Department department){
        return ResultUtil.success(service.insert(department));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Department> departments){
        return ResultUtil.success(service.insertBatch(departments));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody Department department){
        service.delete(department);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<Department> departments){
        service.deleteBatch(departments);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Department department){
        return ResultUtil.success(service.update(department));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Department> departments){
        return ResultUtil.success(service.updateBatch(departments));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Department department){
        return ResultUtil.success(service.selectOne(department));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Department department){
        return ResultUtil.success(service.selectList(department));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Department department){
        return ResultUtil.success(service.selectPage(department));
    }
}
