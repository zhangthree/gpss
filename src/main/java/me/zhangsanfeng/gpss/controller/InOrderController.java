package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.InOrder;
import me.zhangsanfeng.gpss.service.InOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("入库主记录")
@RestController
@RequestMapping("/inorder")
public class InOrderController {

    @Autowired
    private InOrderService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody InOrder inOrder){
        return ResultUtil.success(service.insert(inOrder));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<InOrder> inOrders){
        return ResultUtil.success(service.insertBatch(inOrders));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody InOrder inOrder){
        service.delete(inOrder);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<InOrder> inOrders){
        service.deleteBatch(inOrders);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody InOrder inOrder){
        return ResultUtil.success(service.update(inOrder));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<InOrder> inOrders){
        return ResultUtil.success(service.updateBatch(inOrders));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody InOrder inOrder){
        return ResultUtil.success(service.selectOne(inOrder));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody InOrder inOrder){
        return ResultUtil.success(service.selectList(inOrder));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody InOrder inOrder){
        return ResultUtil.success(service.selectPage(inOrder));
    }
}
