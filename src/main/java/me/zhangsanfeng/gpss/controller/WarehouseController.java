package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Warehouse;
import me.zhangsanfeng.gpss.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("仓库信息")
@RestController
@RequestMapping("/warehouse")
public class WarehouseController {

    @Autowired
    private WarehouseService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Warehouse warehouse){
        return ResultUtil.success(service.insert(warehouse));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Warehouse> warehouses){
        return ResultUtil.success(service.insertBatch(warehouses));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody Warehouse warehouse){
        service.delete(warehouse);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<Warehouse> warehouses){
        service.deleteBatch(warehouses);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Warehouse warehouse){
        return ResultUtil.success(service.update(warehouse));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Warehouse> warehouses){
        return ResultUtil.success(service.updateBatch(warehouses));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Warehouse warehouse){
        return ResultUtil.success(service.selectOne(warehouse));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Warehouse warehouse){
        return ResultUtil.success(service.selectList(warehouse));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Warehouse warehouse){
        return ResultUtil.success(service.selectPage(warehouse));
    }
}
