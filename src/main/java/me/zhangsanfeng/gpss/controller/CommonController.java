package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName:CommonController
 * @Description: 公共Controller
 * @Author:ZhangYao
 * @Date:2018/12/6 22:57
 * @Version:1.0
 */

@Api("公共接口")
@RestController
@RequestMapping("/common")
public class CommonController {

    @Autowired
    private CommonService service;

    @ApiOperation("生成一个订单号")
    @GetMapping("/orderNo/{type}")
    public Result getOrderNo(@PathVariable("type") String orderType){
        return ResultUtil.success(service.getOrderNo(orderType));
    }
}
