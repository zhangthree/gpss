package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Users;
import me.zhangsanfeng.gpss.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("用户信息")
@RestController
@RequestMapping("/user")
public class UsersController {

    @Autowired
    private UsersService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Users user){
        return ResultUtil.success(service.insert(user));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Users> users){
        return ResultUtil.success(service.insertBatch(users));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody Users user){
        service.delete(user);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<Users> users){
        service.deleteBatch(users);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Users user){
        return ResultUtil.success(service.update(user));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Users> users){
        return ResultUtil.success(service.updateBatch(users));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Users user){
        return ResultUtil.success(service.selectOne(user));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Users user){
        return ResultUtil.success(service.selectList(user));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Users user){
        return ResultUtil.success(service.selectPage(user));
    }

    @PostMapping("/login")
    @ApiOperation("用户登录")
    public Result login(@AuthenticationPrincipal Users user) {
        return ResultUtil.success();
    }

    @PostMapping("/logout")
    @ApiOperation("用户登出")
    public Result logout() {
        return ResultUtil.success();
    }
}
