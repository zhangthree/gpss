package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.OutDetail;
import me.zhangsanfeng.gpss.service.OutDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("出库明细")
@RestController
@RequestMapping("/outdetail")
public class OutDetailController {

    @Autowired
    private OutDetailService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody OutDetail outDetail){
        return ResultUtil.success(service.insert(outDetail));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<OutDetail> outDetails){
        return ResultUtil.success(service.insertBatch(outDetails));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody OutDetail outDetail){
        service.delete(outDetail);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<OutDetail> outDetails){
        service.deleteBatch(outDetails);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody OutDetail outDetail){
        return ResultUtil.success(service.update(outDetail));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<OutDetail> outDetails){
        return ResultUtil.success(service.updateBatch(outDetails));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody OutDetail outDetail){
        return ResultUtil.success(service.selectOne(outDetail));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody OutDetail outDetail){
        return ResultUtil.success(service.selectList(outDetail));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody OutDetail outDetail){
        return ResultUtil.success(service.selectPage(outDetail));
    }
}
