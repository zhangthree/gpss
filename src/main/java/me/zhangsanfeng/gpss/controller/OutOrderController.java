package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.OutOrder;
import me.zhangsanfeng.gpss.service.OutOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("出库主记录")
@RestController
@RequestMapping("/outorder")
public class OutOrderController {

    @Autowired
    private OutOrderService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody OutOrder outOrder){
        return ResultUtil.success(service.insert(outOrder));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<OutOrder> outOrders){
        return ResultUtil.success(service.insertBatch(outOrders));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody OutOrder outOrder){
        service.delete(outOrder);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<OutOrder> outOrders){
        service.deleteBatch(outOrders);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody OutOrder outOrder){
        return ResultUtil.success(service.update(outOrder));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<OutOrder> outOrders){
        return ResultUtil.success(service.updateBatch(outOrders));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody OutOrder outOrder){
        return ResultUtil.success(service.selectOne(outOrder));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody OutOrder outOrder){
        return ResultUtil.success(service.selectList(outOrder));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody OutOrder outOrder){
        return ResultUtil.success(service.selectPage(outOrder));
    }
}
