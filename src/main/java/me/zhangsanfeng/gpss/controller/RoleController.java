package me.zhangsanfeng.gpss.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhangsanfeng.gpss.common.entity.Result;
import me.zhangsanfeng.gpss.common.utils.ResultUtil;
import me.zhangsanfeng.gpss.entity.Role;
import me.zhangsanfeng.gpss.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("角色信息")
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService service;

    @PostMapping
    @ApiOperation("增加一条记录")
    public Result save(@RequestBody Role role){
        return ResultUtil.success(service.insert(role));
    }

    @PostMapping("/batch")
    @ApiOperation("批量增加记录")
    public Result saveBatch(@RequestBody List<Role> roles){
        return ResultUtil.success(service.insertBatch(roles));
    }

    @DeleteMapping
    @ApiOperation(value = "删除一条记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result delete(@RequestBody Role role){
        service.delete(role);
        return ResultUtil.success();
    }

    @DeleteMapping("/batch")
    @ApiOperation(value = "批量删除记录",
            notes = "必须传入关联表id，否则级联删除关联表的关联数据失败，产生关联表中的垃圾数据")
    public Result deleteBatch(@RequestBody List<Role> roles){
        service.deleteBatch(roles);
        return ResultUtil.success();
    }

    @PutMapping
    @ApiOperation("更新一条记录")
    public Result update(@RequestBody Role role){
        return ResultUtil.success(service.update(role));
    }

    @PutMapping("/batch")
    @ApiOperation("批量更新记录")
    public Result updateBatch(@RequestBody List<Role> roles){
        return ResultUtil.success(service.updateBatch(roles));
    }

    @PostMapping("/one")
    @ApiOperation("查询一条记录")
    public Result selectOne(@RequestBody Role role){
        return ResultUtil.success(service.selectOne(role));
    }

    @PostMapping("/list")
    @ApiOperation("查询列表")
    public Result selectList(@RequestBody Role role){
        return ResultUtil.success(service.selectList(role));
    }

    @PostMapping("/page")
    @ApiOperation("查询分页列表")
    public Result selectPage(@RequestBody Role role){
        return ResultUtil.success(service.selectPage(role));
    }
}
